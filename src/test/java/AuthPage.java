import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.sleep;

public class AuthPage {
    private final SelenideElement googleAuth = $x("//a[@class=\"h-mod social-icon-button __small __gp\"]");
    public AuthPage(String url) {
        Selenide.open(url);
    }
    public AuthPage clickGoogleAuthButton() {
        googleAuth.click();
        sleep(2000);
        return this;
    }
}
