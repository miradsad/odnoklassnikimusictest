import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$x;

public class MusicSearchPage {
    private final SelenideElement music = $x("//li[@data-l=\"t,music\"]");
    private final SelenideElement songInput = $x("//*[@id=\"music_layer\"]/header/div/wm-search-form/wm-search-input/input");
    public MusicSearchPage jumpToMusic(){
        music.click();
        return new MusicSearchPage();
    }
    public MusicSearchPage searchMusic(String songString){
        songInput.setValue(songString);
        songInput.sendKeys(Keys.ENTER);
        return this;
    }
}
