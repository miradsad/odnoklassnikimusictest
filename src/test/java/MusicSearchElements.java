import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.Assertions;

import static com.codeborne.selenide.Selenide.*;

public class MusicSearchElements {
    private final ElementsCollection mySearchResults = $$x("//wm-tracks-list//wm-track");

    public void playFourthSearchResult() {
        SelenideElement result = mySearchResults.get(4);
        result.click();
        sleep(3000); // Задержка в 4000 миллисекунд
        SelenideElement playback = result.find("wm-playback");
        Assertions.assertTrue(playback.exists() || playback.isDisplayed(), "wm-playback is not present or not displayed");
    }
}