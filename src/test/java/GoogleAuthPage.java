import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.sleep;

public class GoogleAuthPage extends MyTest {
    private final SelenideElement email = $x("//input[@type=\"email\"]");
    private final SelenideElement pass = $x("//input[@type=\"password\"]");

    public GoogleAuthPage inputTheEmail(String emString) {
        email.setValue(emString).sendKeys(Keys.ENTER);
        sleep(3000);
        return this;
    }
    public GoogleAuthPage inputThePassword(String pwString) {
        pass.setValue(pwString).sendKeys(Keys.ENTER);
        sleep(3000);
        return this;
    }
}