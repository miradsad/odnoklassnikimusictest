import com.codeborne.selenide.WebDriverRunner;
import org.junit.jupiter.api.Test;


public class MyTest extends BaseTest {
    private final static String MY_URL = "https://ok.ru/";
    private final static String MY_SONG = "Kiss I Was Made for loving you";
    @Test
    public void checkPlayMusic() {
        AuthPage authPage = new AuthPage(MY_URL);
        authPage.clickGoogleAuthButton();
        String originalWindowHandle = WebDriverRunner.getWebDriver().getWindowHandle();
        System.out.println("Задали оригинальную вкладку");
        for (String windowHandle : WebDriverRunner.getWebDriver().getWindowHandles()) {
            WebDriverRunner.getWebDriver().switchTo().window(windowHandle);
        }
        System.out.println("Переключились на вкладку, которая с авторизацией Google");
        GoogleAuthPage googleAuthPage = new GoogleAuthPage();
        googleAuthPage.inputTheEmail(botFirstLogin).inputThePassword(botFirstPassword);
        WebDriverRunner.getWebDriver().switchTo().window(originalWindowHandle);
        System.out.println("Вернулись к оригинальной вкладке");
        MusicSearchPage musicSearchPage = new MusicSearchPage();
        musicSearchPage.jumpToMusic().searchMusic(MY_SONG);
        MusicSearchElements musicSearchElements = new MusicSearchElements();
        musicSearchElements.playFourthSearchResult();
    }
}
